import * as React from 'react';
import * as PropTypes from 'prop-types';
import {Component} from "react";

export default class CreditCalculationResult extends Component {
    props: {
        creditAmount: number,
        numberOfCreditsAlreadyTaken?: number,
        baseFee?: number,
        deductions?: number,
        annualFee?: number
    };

    static propTypes = {
        creditAmount: PropTypes.number.isRequired,
        numberOfCreditsAlreadyTaken: PropTypes.number,
        baseFee: PropTypes.number,
        deductions: PropTypes.number,
        annualFee: PropTypes.number
    };

    render() {
        return <div>
            <div className="card">
                <div className="card-header">
                    CALCULATION RESULT
                </div>
                <div className="card-body">
                    <p>Requested credit amount: <b>{ this.props.creditAmount }</b></p>
                    <p>Number of credits already taken: <b>{ this.props.numberOfCreditsAlreadyTaken }</b></p>
                    <p>Calculated base fee: <b>{ this.props.baseFee }</b></p>
                    <p>Calculated deductions: <b>{ this.props.deductions }</b></p>
                    <p>Calculated annual fee: <b>{ this.props.annualFee }</b></p>
                </div>
            </div>
        </div>
    }
}