import * as React from 'react';
import * as PropTypes from 'prop-types';
import { Component } from "react";
import {CustomerType} from "../types/CustomerType";
import {parseFloatOr, parseIntegerOr} from "../tools/tools";

export default class CreditForm extends Component {
    props: {
        customerType: CustomerType,
        creditAmount: number,
        numberOfCreditsAlreadyTaken?: number,
        creditParametersChanged: (customerType: CustomerType, creditAmount: number, numberOfCreditsAlreadyTaken?: number) => void
    };

    static propTypes = {
        customerType: PropTypes.number,
        creditAmount: PropTypes.number.isRequired,
        numberOfCreditsAlreadyTaken: PropTypes.number,
        creditParametersChanged: PropTypes.func
    };

    render() {
        return <form>
            <div className="card mb-4">
                <div className="card-header">
                    FORM
                </div>
                <div className="card-body">
                    <div className="form-group">
                        <label htmlFor="credit-form-client-type">Client type</label>
                        <select className="form-control" id="credit-form-client-type" value={this.props.customerType}
                               onChange={e => this.props.creditParametersChanged(parseIntegerOr(e.target.value, CustomerType.Company), this.props.creditAmount, this.props.numberOfCreditsAlreadyTaken)}>
                            <option value={CustomerType.Company}>Company</option>
                            <option value={CustomerType.PhysicalPerson}>Physical person</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="credit-form-credit-amount">Credit amount</label>
                        <input className="form-control" id="credit-form-credit-amount" type="numeric" value={this.props.creditAmount}
                               onChange={e => this.props.creditParametersChanged(this.props.customerType, parseFloatOr(e.target.value, 1), this.props.numberOfCreditsAlreadyTaken)} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="credit-form-number-of-credits">Number of credits already taken</label>
                        <input className="form-control" id="credit-form-number-of-credits" type="numeric" value={this.props.numberOfCreditsAlreadyTaken}
                               onChange={e => this.props.creditParametersChanged(this.props.customerType, this.props.creditAmount, parseIntegerOr(e.target.value, 0))} />
                    </div>
                </div>
            </div>
          </form>
    }
}
