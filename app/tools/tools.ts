function isConvertibleToFloat(n) {
    n = parseFloat(n);
    return (n === +n);
}

function isConvertibleToInteger(n) {
    n = parseInt(n);
    return n === +n && n === (n|0);
}

export function parseFloatOr(n, or) {
    return isConvertibleToFloat(n) ? parseFloat(n) : or;
}

export function parseIntegerOr(n, or) {
    return isConvertibleToInteger(n) ? parseInt(n) : or;
}