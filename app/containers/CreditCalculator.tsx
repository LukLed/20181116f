import * as React from 'react';
import {Component} from 'react';
import * as PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {fetchCreditCalculation} from '../actions';
import CreditForm from '../components/CreditForm';
import CreditCalculationResult from '../components/CreditCalculationResult';
import {CustomerType} from "../types/CustomerType";

class CreditCalculator extends Component {
    props: {
        customerType: CustomerType,
        creditAmount: number,
        numberOfCreditsAlreadyTaken?: number,
        creditParametersChanged: (creditAmount: number, numberOfCreditsAlreadyTaken: number) => void,
        dispatch: (action: any) => void,
        baseFee?: number,
        deductions?: number,
        annualFee?: number
    };

    static propTypes = {
        customerType: PropTypes.number.isRequired,
        creditAmount: PropTypes.number.isRequired,
        numberOfCreditsAlreadyTaken: PropTypes.number,
        creditParametersChanged: PropTypes.func,
        baseFee: PropTypes.number,
        deductions: PropTypes.number,
        annualFee: PropTypes.number
    };

    creditParametersChanged = (customerType, creditAmount, numberOfCreditsAlreadyTaken) => {
        this.props.dispatch(fetchCreditCalculation(customerType, creditAmount, numberOfCreditsAlreadyTaken));
    };

    render() {
        return (
            <div className="row">
                <div className="col-sm-4 offset-sm-4">
                    <h1>Credit calculator</h1>
                    <CreditForm customerType={this.props.customerType} creditAmount={this.props.creditAmount} creditParametersChanged={this.creditParametersChanged} numberOfCreditsAlreadyTaken={this.props.numberOfCreditsAlreadyTaken} />
                    <CreditCalculationResult
                        creditAmount={this.props.creditAmount}
                        numberOfCreditsAlreadyTaken={this.props.numberOfCreditsAlreadyTaken}
                        annualFee={this.props.annualFee}
                        baseFee={this.props.baseFee}
                        deductions={this.props.deductions}
                    />
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { creditForm } = state;

    let result = {
        customerType: creditForm.customerType,
        creditAmount: creditForm.creditAmount,
        numberOfCreditsAlreadyTaken: creditForm.numberOfCreditsAlreadyTaken,
        creditParametersChanged: creditForm.creditParametersChanged,
        dispatch: creditForm.dispatch,
        baseFee: creditForm.baseFee,
        deductions: creditForm.deductions,
        annualFee: creditForm.annualFee
    };

    return result;
};

export default connect(mapStateToProps)(CreditCalculator)
