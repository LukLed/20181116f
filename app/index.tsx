import * as React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import reducer from './reducers';
import CreditCalculator from './containers/CreditCalculator';
import {CustomerType} from "./types/CustomerType";

const middleware = [ thunk ];

if (process.env.NODE_ENV !== 'production') {
    middleware.push(createLogger())
}

const store = createStore(
    reducer,
    {
        creditForm: {
            creditAmount: 50000,
            customerType: CustomerType.Company,
            numberOfCreditsAlreadyTaken: 0
        }
    },
    applyMiddleware(...middleware)
);

render(
    <Provider store={store}>
        <CreditCalculator />
    </Provider>,
    document.getElementById('root')
);