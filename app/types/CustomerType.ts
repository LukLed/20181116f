export enum CustomerType {
    Company = 1,
    PhysicalPerson = 2
};