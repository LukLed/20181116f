import {combineReducers} from 'redux';
import {REQUEST_CREDIT_CALCULATION, RESPONSE_CREDIT_CALCULATION} from '../actions';

const creditForm = (state = {}, action) => {
    switch (action.type) {
        case REQUEST_CREDIT_CALCULATION:
            return {
                ...state,
                creditAmount: action.creditAmount,
                numberOfCreditsAlreadyTaken: action.numberOfCreditsAlreadyTaken,
                customerType: action.customerType
            };
        case RESPONSE_CREDIT_CALCULATION:
            return {
                ...state,
                customerType: action.customerType,
                creditAmount: action.creditAmount,
                numberOfCreditsAlreadyTaken: action.numberOfCreditsAlreadyTaken,
                baseFee: action.baseFee,
                deductions: action.deductions,
                annualFee: action.annualFee
            };
        default:
            return state;
    }
};

const rootReducer = combineReducers({
    creditForm
});

export default rootReducer
