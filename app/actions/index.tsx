import {CustomerType} from "../types/CustomerType";

export const REQUEST_CREDIT_CALCULATION = 'REQUEST_CREDIT_CALCULATION';
export const RESPONSE_CREDIT_CALCULATION = 'RESPONSE_CREDIT_CALCULATION';

export const requestCreditCalculation = (customerType, creditAmount, numberOfCreditsAlreadyTaken) => ({
    type: REQUEST_CREDIT_CALCULATION,
    customerType,
    creditAmount,
    numberOfCreditsAlreadyTaken
});

export const responseCreditCalculation = (customerType, creditAmount, numberOfCreditsAlreadyTaken, baseFee, deductions, annualFee) => ({
    type: RESPONSE_CREDIT_CALCULATION,
    customerType,
    creditAmount,
    numberOfCreditsAlreadyTaken,
    baseFee,
    deductions,
    annualFee
});

export const fetchCreditCalculation = (customerType, creditAmount, numberOfCreditsAlreadyTaken) => dispatch => {
    dispatch(requestCreditCalculation(customerType, creditAmount, numberOfCreditsAlreadyTaken));
    return fetch('https://creditcalculationsapi20181118030307.azurewebsites.net/api/FeeCalculations/Calculate',{
        method: 'post',
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify({
            customerType: customerType,
            creditAmount,
            numberOfCreditsAlreadyTaken
        })
    }).then(response => response.json(), response => {
        // needs proper error handling
        alert('Could not retrieve fee.');
        dispatch(responseCreditCalculation(CustomerType.Company, 1, 0, null, null, null))
    })
    .then(json =>
        dispatch(responseCreditCalculation(customerType, creditAmount, numberOfCreditsAlreadyTaken, json.baseFee, json.deductions, json.annualFee)))
}
